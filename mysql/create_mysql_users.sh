#!/bin/bash

# Purpose of the script:
# - Generate MySQL-User and Passwords with a userlist
# - Grant all permissions for Databases with User-Prefix
# - Grant select permissions for Databases from Uni-Bayreuth
# Take a look at https://dbup2date.uni-bayreuth.de/
# You need a userlist and start this script with the list as the first Parameter
# The needed list structure:
# <username>,<password>
# One User per line
# Call the script like that:
# ./create_mysql_users userlist.txt

function writeInDB() {
    mysql -u root -p$rootpassword << EOF
    use mysql
    create user '$username'@'%' IDENTIFIED BY '$password';
    grant all privileges on \`$username\_%\`.* to '$username'@'%';
    grant select on bundesliga.* to '$username'@'%';
    grant select on wetterdaten.* to '$username'@'%';
    flush privileges;
EOF
}

echo "Passwort für MariaDB-root-Zugriff"
read -s rootpassword

for line in $(cat $1);do
    username=$(echo $line | cut -d, -f1)
    password=$(echo $line | cut -d, -f2)
    echo Create User $username with password $password
    writeInDB
done
