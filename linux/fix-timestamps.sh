#!/bin/bash

# Prep: Create Snapshot on VM if it is possible
# Use: fix-timestamps.sh <orig-files> <copied-files>

### Settings

#Absolut Paths of given Parameters
ori_files=$(readlink -f $1)
new_files=$(readlink -f $2)

#Internal Field Separator is only new line
IFS=$'\n'

#Files newer than ref-time (24.07.21 - 21:30) will not be touched
#ref_time=1627155000
ref_time=1627641361 # Just for testing the script

### Functions

function fix_subdir_timestamps() {
    new_dir=$new_files${1:1} #Leading Dot of $1 is extracted
    ori_dir=$ori_files${1:1} #Leading Dot of $1 is extracted
    echo "-----"
    echo "New Dir: $new_dir"
    echo "Old Dir: $ori_dir"
    for i in $(ls $new_dir);do
        if [ -e $ori_dir/$i ]; then
            file_time=$(stat --format='%Y' $new_dir/$i)
            if (($file_time < $ref_time));then
                echo "$i: Alter Zeitstempel wird übernommen!"
                touch -r $ori_dir/$i $new_dir/$i
            else
                echo "$i: Datei bereits bearbeitet."
            fi
        else
            echo "$i: Datei neu erstellt"
        fi
    done
}

### Script Start

cd $new_files
for i in $(find . -type d);do
    fix_subdir_timestamps "$i"
done
