#!/bin/bash

# Call that script as root with a list of IPs as Argument
# The list must contain one IPv4-Address per line

targets=$1
online=0

function checkuser() {
    if [ "$(whoami)" != root ]; then
        echo "Run this script as root."
        exit 1
    fi
}

function checkhost() {
    while true; do
        clear
        echo -e "IPs \t\tStatus  \tOpenPorts"
        while read -r host; do

            # Check Host via ping
            if ping -c1 -W1 "$host" > /dev/null; then
                online=1
            else
                online=0
            fi

            # Check Host via nmap and save results in an array
            nmap -sS -oN output.txt "$host" > /dev/null
            ports=()
            while read -r line; do
                  if [[ $line == *"open"* ]];then
                      ports+=("$(echo "$line" | cut -d'/' -f1)")
                  fi
            done < output.txt

            # Determine online or offline
            if [ ${#ports[@]} == 0 ] && [ $online == 0 ] ; then
                online=0
            else
                online=1
            fi

            # Print line 
            if [ "$online" -eq 0 ]; then
                echo -en "$host \t\033[1;31m[ offline ]\033[0m\t"
                echo "NONE"
            else
                echo -ne "$host \t\033[1;32m[ online  ]\033[0m\t"
                echo "${ports[@]}"
            fi

        done < "$targets"
        #echo "$buffer"
        sleep 10
    done
}

checkuser
checkhost
