#!/bin/bash
#
# Analyze "no bootable device" on acer notebook
#
# Idea: 
# - Every boot, the system waits a few secs
# - and restart or shutdown the device
# - so I can provoke failed boot message with a minimum of user action
#
# Add following line in crontab: @reboot /path/to/this/script 

file=latest-action.txt
sleep 120

# Init file
if [ ! -f $file ];then
	echo reboot > "$file"
fi

# Change shutdown-command on every call
if [ $(cat $file) == "reboot" ]; then
	echo Letzter Status ist \"reboot\"
	echo shutdown > "$file"
	shutdown -r now
else
	echo Letzter Status ist \"shutdown\"
	echo reboot > "$file"
	shutdown -h now
fi
