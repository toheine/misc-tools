#!/bin/bash

# Purpose of the script:
# You can run this script from a desktop-starter for a normal 
# user to change the LUKS-Key on Slot 1. It contains a simple 
# description and a feedback of success or fail.

# Check if the script is executed as root
if [ $(whoami) != root ]; then
    echo "Run this script as root."
    exit 1
fi

echo "+-------------------------------------------------+"
echo "|  Passwort für verschlüsselte Festplatte ändern  |"
echo "|                                                 |"
echo "|  Hinweis: In der nachfolgenden Aufforderung zur |"
echo "|  Passwortänderung wird die Tastatureingabe      |"
echo "|  NICHT angezeigt!                               |"
echo "+-------------------------------------------------+"

cryptsetup luksChangeKey -S 1 /dev/nvme0n1p3

if [ $(echo $?) -eq 0 ]; then
    echo -e "\033[1;32m Dein Passwort wurde geändert!\033[0m"
else
    echo -e "\033[1;31m Es ist ein Fehler aufgetreten!\033[0m"
fi

echo "Das Fenster schließt sich in 3 Sekunden!"
sleep 3
