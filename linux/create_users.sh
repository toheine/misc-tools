#!/bin/bash

# Purpose of the script:
# Generate Linux-User and Passwords with a userlist and put the user in a given group
# You need a userlist and start this script with the list as the first Parameter
# The group is the second parameter
# The list structure:
# <username>,<password>
# One User per line
# Call the script like that:
# sudo ./create_users.sh userlist.txt class10b

userlist=$1
group=$2

function check_root() {
    if [ "$(whoami)" != root ]; then
        echo "Run this script as root."
        exit 1
    fi
}

function check_group() {
    if grep -q "^$group" /etc/group
    then
        echo "The group $group exists"
    else
        echo "The group $group does not exist. I create the group now"
        groupadd "$group"
    fi
}

function add_users() {
    while read -r line
    do
        username=$(echo "$line" | cut -d, -f1)
        password=$(echo "$line" | cut -d, -f2)
        echo ---
        echo "$username"
        echo "$password"
        useradd -s /bin/bash -m "$username"
        chpasswd <<< "$username:$password"
        usermod -aG "$group" "$username"
    done < "$userlist"
}

check_root
check_group
add_users
