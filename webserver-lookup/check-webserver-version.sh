#!/bin/bash

for line in $(cat $1); do
    echo "curl -s --head https://$line | grep server"
    curl -s --head https://$line | grep server
    echo "---"
done
